package com.devcamp.preskill;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

/**
 */
public class NiceExcelWriterExample {
	//khai báo phương thức writeExcel để tạo file excel
	public void writeExcel(List<Book> listBook, String excelFilePath) throws IOException {
		Workbook workbook = new HSSFWorkbook();//tạo file mới
		Sheet sheet = workbook.createSheet();//tạo worksheet mới
		//tham số lưu số dòng dữ liệu
		int rowCount = 0;
		//với mỗi giá trị của mảng data thì thêm dòng và gọi hàm writeBook để điền book data 
		for (Book aBook : listBook) {
			Row row = sheet.createRow(++rowCount);
			writeBook(aBook, row);
		}
		
		try (FileOutputStream outputStream = new FileOutputStream(excelFilePath)) {
			workbook.write(outputStream);
		}		
	}
	
	private void writeBook(Book aBook, Row row) {
		Cell cell = row.createCell(1);//tạo ô dữ liệu vào dòng hiện tại ở cột 1
		cell.setCellValue(aBook.getTitle());//thêm data của thuộc tính title

		cell = row.createCell(2);// tạo ô dữ liệu vào dòng hiện tại ở cột 2
		cell.setCellValue(aBook.getAuthor());// thêm data của thuộc tính author
		
		cell = row.createCell(3);// tạo ô dữ liệu vào dòng hiện tại ở cột 3
		cell.setCellValue(aBook.getPrice());// thêm dữ liệu của thuộc tính Price
	}
	
	private List<Book> getListBook() {
		Book book1 = new Book("Head First Java", "Kathy Serria", 79);
		Book book2 = new Book("Effective Java", "Joshua Bloch", 36);
		Book book3 = new Book("Clean Code", "Robert Martin", 42);
		Book book4 = new Book("Thinking in Java", "Bruce Eckel", 35);
		
		List<Book> listBook = Arrays.asList(book1, book2, book3, book4);
		
		return listBook;
	}
	
	public static void main(String[] args) throws IOException {
		NiceExcelWriterExample excelWriter = new NiceExcelWriterExample(); //tạo file excel mới
		List<Book> listBook = excelWriter.getListBook(); //lấy danh sách mảng cần hiển thị vào nội dung file
		String excelFilePath = "NiceJavaBooks.xls"; //đặt tên file
		excelWriter.writeExcel(listBook, excelFilePath); //gọi phương thức writeExcel và truyền vào nội dung và đường dẫn
	}

}
