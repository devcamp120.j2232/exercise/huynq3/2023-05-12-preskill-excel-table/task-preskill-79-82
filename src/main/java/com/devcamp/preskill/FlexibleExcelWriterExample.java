package com.devcamp.preskill;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * A flexible program that writes data to an Excel file in either
 * XLSX or XLS format, depending on the extension of the file. 
 *
 */
public class FlexibleExcelWriterExample {

	public void writeExcel(List<Book> listBook, String excelFilePath) throws IOException {
		Workbook workbook = getWorkbook(excelFilePath);//tao workbook
		Sheet sheet = workbook.createSheet();//tạo sheet
		//khai báo biến lưu số data row
		int rowCount = 0;
		//thêm data từ list vào sheet
		for (Book aBook : listBook) {
			Row row = sheet.createRow(++rowCount); //thêm data row
			writeBook(aBook, row); //gọi hàm hiển thị data vào row
		}
		
		try (FileOutputStream outputStream = new FileOutputStream(excelFilePath)) {
			workbook.write(outputStream);
		}		
	}
	//hàm hiển thị data vào row
	private void writeBook(Book aBook, Row row) {
		Cell cell = row.createCell(1); //thêm data cell ở cột 1
		cell.setCellValue(aBook.getTitle()); //lấy thông tin title điền vào data cell
		//thêm data cell ở cột 2 và điền thông tin author
		cell = row.createCell(2);
		cell.setCellValue(aBook.getAuthor());
		//thêm data cell ở cột 3 và điền thông tin price
		cell = row.createCell(3);
		cell.setCellValue(aBook.getPrice());
	}
	//lấy danh sách Book
	private List<Book> getListBook() {
		Book book1 = new Book("Head First Java", "Kathy Serria", 79);
		Book book2 = new Book("Effective Java", "Joshua Bloch", 36);
		Book book3 = new Book("Clean Code", "Robert Martin", 42);
		Book book4 = new Book("Thinking in Java", "Bruce Eckel", 35);
		
		List<Book> listBook = Arrays.asList(book1, book2, book3, book4);
		
		return listBook;
	}
	
	private Workbook getWorkbook(String excelFilePath) 
			throws IOException {
		Workbook workbook = null;
		
		if (excelFilePath.endsWith("xlsx")) {
			workbook = new XSSFWorkbook();
		} else if (excelFilePath.endsWith("xls")) {
			workbook = new HSSFWorkbook();
		} else {
			throw new IllegalArgumentException("The specified file is not Excel file");
		}
		
		return workbook;
	}
	
	public static void main(String[] args) throws IOException {
		FlexibleExcelWriterExample excelWriter = new FlexibleExcelWriterExample();//khởi tạo hàm tạo file excel
		List<Book> listBook = excelWriter.getListBook();//lấy danh sách cần hiển thị
		String excelFilePath = "JavaBooks1.xls";// khai báo tên file
		excelWriter.writeExcel(listBook, excelFilePath); //gọi hàm hiển thị thông tin lê file đã tạo
		//khai báo tên file 2 và hiển thị thông tin lên file 2
		excelFilePath = "JavaBooks2.xlsx";
		excelWriter.writeExcel(listBook, excelFilePath);
	}

}
